gamestart=false
player = {}
player.__index=player
function player:new(x,y,w,h)
	return setmetatable({score=0,x=x,y=y,w=w,h=h,up=false,down=false},player)
end
function player:draw()
	love.graphics.setColor(1,1,1)
	love.graphics.rectangle("fill",self.x,self.y,self.w,self.h)
	love.graphics.setColor(0,0,0)
	love.graphics.print(self.score,self.x+(self.w/2),self.y+(self.h/2))
end
function player:update(dt)
	if self.up and self.y > 0 then
		self.y=self.y-(dt*1000)
	end
	if self.down and self.y+self.h<480 then
		self.y=self.y+(dt*1000)
	end
end
function player:collide(other)
	return other.x <= self.x+self.w and self.x <= other.x+other.w and  other.y <= self.y+self.h and self.y <= other.y+other.h
end
ball={dx=0,dy=0,w=60,h=60,x=(640/2)-30,y=(480/2)-30}
function ball:draw()
	love.graphics.setColor(1,1,1)
	love.graphics.rectangle("fill",self.x,self.y,self.w,self.h)
end
function ball:update(dt)
	self.x=self.x+(self.dx*dt)
	self.y=self.y+(self.dy*dt)
	if (self.y<0 and self.dy<0) or (self.y+self.h>480 and self.dy>0) then
		self.dy=-self.dy
	end
	if(self.x<0) then
		p2.score=p2.score+1
		reset()
	end
	if(self.x+self.w>640) then
		p1.score=p1.score+1
		reset()
	end
end
function reset()
	gamestart=false
	ball.x=(640/2)-30
	ball.y=(480/2)-30
	local angle=math.random(0,2*math.pi)
	ball.dx=500*math.cos(angle)
	ball.dy=500*math.sin(angle)
end
function love.load()
	p1=player:new(0,0,60,180)
	p2=player:new(640-60,0,60,180)
	reset()
end
function love.update(dt)
	if not gamestart then
		return
	end
	p1:update(dt)
	p2:update(dt)
	ball:update(dt)
	if p1:collide(ball) then
		local angle = 75*((ball.y+(ball.h/2)-p1.y-(p1.h/2))/(p1.y+p1.h+(ball.h/2)-p1.y-(p1.h/2)))
		angle=math.rad(angle)
		ball.dx=500*math.cos(angle)
		ball.dy=500*math.sin(angle)
	end
	if p2:collide(ball) then
		local angle = 180+(-75*((ball.y+(ball.h/2)-p2.y-(p2.h/2))/(p2.y+p2.h+(ball.h/2)-p2.y-(p2.h/2))))
		angle=math.rad(angle)
		ball.dx=500*math.cos(angle)
		ball.dy=500*math.sin(angle)
	end
end
function love.draw()
	p1:draw()
	p2:draw()
	ball:draw()
end
function love.keypressed(key)
	gamestart=true
	if key=="w" then
		p1.up=true
	end
	if key=="s" then
		p1.down=true
	end
	if key=="up" then
		p2.up=true
	end
	if key=="down" then
		p2.down=true
	end
end
function love.keyreleased(key)
	if key=="w" then
		p1.up=false
	end
	if key=="s" then
		p1.down=false
	end
	if key=="up" then
		p2.up=false
	end
	if key=="down" then
		p2.down=false
	end
end
